FROM rustembedded/cross:aarch64-linux-android

RUN dpkg --add-architecture arm64 && \
    apt-get update && \
    apt-get install --assume-yes libfontconfig1-dev:arm64 \
	libicu-dev:arm64 \
	libssl-dev:arm64 \
	zlib1g-dev:arm64 \
	libssl-dev \
	pkg-config \
	libfontconfig1-dev \
	libgraphite2-dev \
	libharfbuzz-dev \
	libicu-dev \
	zlib1g-dev

ENV PKG_CONFIG_ALLOW_CROSS 1
